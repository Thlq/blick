import {
  classicBeh
} from '../classic-beh.js'

const mMgr = wx.getBackgroundAudioManager()

Component({
  /**
   * 组件的属性列表
   */
  behaviors: [classicBeh],
  properties: {
    src: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    pauseSrc: './images/pause.png',
    playSrc: './images/play.png',
    playing: false
  },

  attached () {
    this._recoverStatus()
    this._monitorSwitch()
  },
  detached () {

  },
  /**
   * 组件的方法列表
   */
  methods: {
    onPlay (enevt) {
      if (!this.data.playing) {
        this.setData({
          playing: true
        })
        if(mMgr.src==this.properties.src){
          mMgr.play()
        }else{
          mMgr.src = this.properties.src
          mMgr.title = '猪猪侠之声'
        }
      } else {
        this.setData({
          playing: false
        })
        mMgr.pause()
      }
    },
    _recoverStatus () {
      if (mMgr.paused) {
        this.setData({
          playing: false
        })
        return
      }
      if (mMgr.src == this.properties.src) {
        this.setData({
          playing: true
        })
      }
    },
    _monitorSwitch () {
      mMgr.onPlay(() => {
        this._recoverStatus()
      })
      mMgr.onPause(() => {
        this._recoverStatus()
      })
      mMgr.onStop(() => {
        this._recoverStatus()
      })
      mMgr.onEnded(() => {
        this._recoverStatus()
      })
    }
  }
})