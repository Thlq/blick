import {
  HTTP
} from '../util/http.js'

class ClassicModel extends HTTP {
  getLatest(sCallback) {
    this.request({
      url: '/classic/latest',
      success: (res) => {
        wx.setStorageSync(this._getKey(res.index), res.data)
        this._setLatestIndex(res.data.index)
        sCallback(res.data);
      }
    })
  }

  getClassic(index, nextOrPrevious, sCallback) {
    let key = nextOrPrevious == 'next' ? this._getKey(index + 1) : this._getKey(index - 1)
    let classic = wx.getStorageSync(key)
    if (!classic) {
      this.request({
        url: `/classic/${nextOrPrevious}?index=${index}`,
        success: (res) => {
          wx.setStorageSync(this._getKey(res.data.index), res.data)
          sCallback(res.data);
        }
      })
    } else {
      sCallback(classic);
    }
  }

  isFirst(index) {
    return index == 1 ? true : false
  }

  isLatest(index) {
    let latsetIndex = this._getLatestIndex()
    return index == latsetIndex ? true : false
  }

  _setLatestIndex(value) {
    wx.setStorageSync('latest', value)
  }

  _getLatestIndex() {
    let index = wx.getStorageSync('latest')
    return index
  }

  _getKey(index) {
    let key = `classic-${index}`
    return key
  }
}

export {
  ClassicModel
}