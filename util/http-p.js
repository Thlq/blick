import {
  config
} from '../config.js'

const tips = {
  1: '默认错误',
  1005: '1005'
}

class HTTP {
  constructor() {
    this.baseRestUrl = config.api_base_url
  }
  request({url,data={},method="GET"}){
    return new Promise((resolve,reject)=>{
      this._request(url,resolve,reject,data,method)
    })
  }
  _request(url,resolve,reject,data,method) {
    url = this.baseRestUrl + url
    wx.request({
      url: url,
      method: method,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        const code = res.statusCode.toString()
        if (code.startsWith('2')) {
          resolve(res.data)
        } else {
          reject()
          const error_code = res.data.error_code
          this._show_error(error_code)
        }
      },
      fail: (err) => {
        reject()
        this._show_error(1)
      }
    })
  }
  _show_error(error_code) {
    if (!error_code) {
      error_code = 1
    }
    const tip = tips[error_code]?tips[error_code]:"系统异常"
    wx.showToast({
      title: tip,
      icon: 'none',
      duration: 2000
    })
  }
}

export {
  HTTP
}