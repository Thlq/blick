import {
  config
} from '../config.js'

const tips = {
  1: '默认错误',
  1005: '1005'
}

class HTTP {
  constructor() {
    this.baseRestUrl = config.api_base_url
  }
  request(params) {
    let url = this.baseRestUrl + params.url
    if (!params.method) {
      params.method = 'GET'
    }
    wx.request({
      url: url,
      method: params.method,
      data: params.data,
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        let code = res.statusCode.toString()
        if (code.startsWith('2')) {
          params.success && params.success(res.data)
        } else {
          let error_code = res.data.error_code
          this._show_error(error_code)
        }
      },
      fail: (err) => {
        this._show_error(1)
      }
    })
  }
  _show_error(error_code) {
    if (!error_code) {
      error_code = 1
    }
    wx.showToast({
      title: tips[error_code],
      icon: 'none',
      duration: 2000
    })
  }
}

export {
  HTTP
}